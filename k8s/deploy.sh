kubectl create namespace ${CI_COMMIT_BRANCH}
kubectl apply -f mysql-secret.yaml -n ${CI_COMMIT_BRANCH}
kubectl apply -f mysql.yaml -n ${CI_COMMIT_BRANCH}
kubectl apply -f app.yaml -n ${CI_COMMIT_BRANCH}
helm install ingress-nginx ./k8s/ingress-nginx/
kubectl apply -f ingress.yaml -n ${CI_COMMIT_BRANCH}