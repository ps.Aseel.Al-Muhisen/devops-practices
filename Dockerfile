From openjdk:8-jre-alpine
EXPOSE 8070
ENV DB_PROFILE h2
ENV JAVA_OPTS "-Xms64m -Xmx128m -Djava.security.egd=file:/dev/./urandom"
WORKDIR /u01/DevOps-Practice/
COPY ./target/assignment-*.jar /u01/DevOps-Practice/MyApp.jar
CMD java $JAVA_OPTS -Dserver.port=8070 -Dspring.profiles.active=$DB_PROFILE -jar MyApp.jar